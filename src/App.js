import ComponentTime from "./ComponentTime";


function App() {
  return (
    <div >
      <h1>Hello, world!</h1>
      <ComponentTime></ComponentTime>
    </div>
  );
}

export default App;
