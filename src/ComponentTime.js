import { Component } from "react";

class ComponentTime extends Component{
    constructor (props){
        super(props)
        this.state = {date : new Date(),color: ""}
    }

    onBtnClick = ()=>{
       
        if (this.state.date.getSeconds()%2 ==0){
            this.setState({color: "blue"})
        }
        else (this.setState({color: "red"}))
    }
    render(){
        setInterval(()=>{
            this.setState({date : new Date()})
        },1000)
        return(
            <div>
                <p   style={{color:this.state.color}}>It is {this.state.date.getHours()%12} : {this.state.date.getMinutes()} : {this.state.date.getSeconds()>9?this.state.date.getSeconds():"0"+this.state.date.getSeconds()} {this.state.date.getHours()>12?"PM":"AM"}</p>
                <button onClick={this.onBtnClick}>Change Color</button>
            </div>
        )
    }
}
export default ComponentTime